const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Route for checking email
router.post("/checkEmail", userController.checkEmailExists);

// Route for registration
router.post(
  "/register",
  userController.checkEmailExists,
  userController.registerUser
);

// Route for login
router.post("/login", userController.loginUser);

// Route for user details
router.post("/details", auth.verify, userController.getProfile);

//
router.get("/profile", userController.profileDetails);

// Update role
router.patch("/updateRole/:userId", auth.verify, userController.updateRole);

// Enrollment
router.post("/enroll/:courseId", auth.verify, userController.enroll);

module.exports = router;
