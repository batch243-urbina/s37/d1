const Course = require("../models/Course");
const auth = require("../auth");

/*
	Steps:
		1. Create a new course object using mongoose model and information from the request of the user
		2. Save the new course to the database
*/
module.exports.addCourse = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  let newCourse = new Course({
    name: request.body.name,
    description: request.body.name,
    price: request.body.price,
    slots: request.body.slots,
  });

  if (userData.isAdmin) {
    return newCourse
      .save()
      .then((course) => {
        console.log(course);
        response.send(true);
      })
      .catch((error) => {
        console.log(error);
        response.send(false);
      });
  } else {
    return response.send(`You must be an admin to add a course!`);
  }
};

// Retrieve all active courses

module.exports.getAllActive = (request, response) => {
  return Course.find({
    isActive: true,
  })
    .then((result) => {
      response.send(result);
    })
    .catch((err) => {
      response.send(err);
    });
};

// Retrieving a specific course

module.exports.getCourse = (request, response) => {
  const courseId = request.params.courseId;

  return Course.findById(courseId)
    .then((result) => {
      response.send(result);
    })
    .catch((err) => {
      response.send(err);
    });
};

// Update a course

module.exports.updateCourse = (request, response) => {
  const token = request.headers.authorization;
  const userData = auth.decode(token);
  console.log(userData);
  let updatedCourse = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    slots: request.body.slots,
  };

  const courseId = request.params.courseId;

  if (userData.isAdmin) {
    return Course.findByIdAndUpdate(courseId, updatedCourse, {
      new: true,
    })
      .then((result) => {
        response.send(result);
      })
      .catch((err) => {
        response.send(err);
      });
  } else {
    return response.send(`You don't have access to this page`);
  }
};

// Archive a course

module.exports.archiveCourse = (request, response) => {
  const token = request.headers.authorization;
  const userData = auth.decode(token);
  console.log(userData);

  let archiveCourse = {
    isActive: request.body.isActive,
  };

  const courseId = request.params.courseId;

  if (userData.isAdmin) {
    return Course.findByIdAndUpdate(courseId, archiveCourse, { new: true })
      .then((result) => {
        response.send(true);
      })
      .catch((err) => {
        response.send(err);
      });
  } else {
    return response.send(`You are not authorized to access this page`);
  }
};

// Retrieve all courses including inactive

module.exports.getAllCourses = (request, response) => {
  const token = request.headers.authorization;
  const userData = auth.decode(token);
  if (!userData.isAdmin) {
    return response.send(`Sorry, you don't have access to this page`);
  } else {
    return Course.find({})
      .then((result) => response.send(result))
      .catch((err) => {
        console.log(err);
        response.send(err);
      });
  }
};
