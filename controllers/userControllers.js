// const { modelName } = require("../models/User");
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exists
/* 
	Steps:
		1. Use mongoose "find" method to find duplicate emails
		2. Use the "then" method to send a response to the frontend application based on the result of the find method
*/

module.exports.checkEmailExists = (request, response, next) => {
  // The result is sent back to the front end via the then method
  return User.find({ email: request.body.email }).then((result) => {
    console.log(request.body.email);
    let message = ``;
    // The find method returns an array record of matching documents
    if (result.length > 0) {
      message = `The ${request.body.email} is already taken, please use other email`;
      return response.send(message);
    }
    // No duplicate email
    //The email is not yet registered in the database
    else {
      // message = `The email ${request.body.email} is not yet taken`;
      // return response.send(message);
      next();
    }
  });
};

module.exports.registerUser = (request, response) => {
  // Creates a variable "newUser" and instantiates a new 'User' object using mongoose model
  // Uses the information from request body
  let newUser = new User({
    firstName: request.body.firstName,
    lastName: request.body.lastName,
    email: request.body.email,
    // Salt - salt rounds that bcrypt algorithm will run to encrypt the password
    password: bcrypt.hashSync(request.body.password, 10),
    mobileNo: request.body.mobileNo,
  });

  // Saves the created object to our database
  return newUser
    .save()
    .then((user) => {
      console.log(user);
      response.send(
        `Congratulations, sir/ma'am ${newUser.firstName}! You are now successfully registered`
      );
    })
    .catch((error) => {
      console.log(error);
      response.send(
        `Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`
      );
    });
};

// User Authentication

/*
	Steps:
		1. Check database if the user email exist
		2. Compare the password provided in the login form with the password stored in the database
*/

module.exports.loginUser = (request, response) => {
  // The findOne method, returns the first record in the collection that matches the search criteria

  return User.findOne({
    email: request.body.email,
  }).then((result) => {
    console.log(result);
    if (result === null) {
      response.send(`No existing email. Register first`);
    } else {
      // Create the variable to return the result of comparing the passwords from login from and database password
      // compareSync() method is used to compare a non encrypted password from login from to encrypted password retrieved. It will return true or false value depending on the result
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        let token = auth.createAccessToken(result);
        console.log(token);
        return response.send({ accessToken: token });
      } else {
        return response.send(`Incorrect password, please try again.`);
      }
    }
  });
};

// Retrieve the details of a User

module.exports.getProfile = (request, response) => {
  return User.findOne({
    _id: request.body.id,
  }).then((result) => {
    console.log(result);
    if (result === null) {
      return response.send(`User not found!`);
    } else {
      result.password = "";
      return response.send(result);
    }
  });
};

// GET PROFILE DETAILS

module.exports.profileDetails = (request, response) => {
  // User will be object that contains the id and email of the user that is currently logged in
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);
  return User.findById(userData.id)
    .then((result) => {
      result.password = "Confidential";
      return response.send(result);
    })
    .catch((err) => {
      return response.send(err);
    });
};

// UPDATE ROLE

module.exports.updateRole = (request, response) => {
  let token = request.headers.authorization;
  let userData = auth.decode(token);

  let idToBeUpdated = request.params.userId;

  if (userData.isAdmin) {
    return User.findById(idToBeUpdated)
      .then((result) => {
        let update = { isAdmin: !result.isAdmin };

        return User.findByIdAndUpdate(idToBeUpdated, update, {
          new: true,
        })
          .then((document) => {
            document.password = "Confidential";
            response.send(document);
          })
          .catch((err) => {
            response.send(err);
          });
      })
      .catch((err) => response.send(err));
  } else {
    return response.send(`YOu don't have access on this page.`);
  }
};

// Enrollment

module.exports.enroll = async (request, response) => {
  const token = request.headers.authorization;
  let userData = auth.decode(token);

  if (!userData.isAdmin) {
    let courseId = request.params.courseId;

    let data = {
      courseId: courseId,
      userId: userData.id,
    };

    let isCourseUpdated = await Course.findById(courseId)
      .then((result) => {
        result.enrollees.push({
          userId: data.userId,
        });
        result.slots -= 1;

        return result
          .save()
          .then((success) => {
            return true;
          })
          .catch((err) => false);
      })
      .catch((err) => {
        console.log(err);
        return response.send(false);
      });

    let isUserUpdated = await User.findById(data.userId)
      .then((result) => {
        result.enrollments.push({
          courseId: data.courseId,
        });

        return result
          .save()
          .then((success) => {
            return true;
          })
          .catch((err) => {
            return false;
          });
      })
      .catch((err) => {
        console.log(err);
        return response.send(false);
      });

    isUserUpdated && isCourseUpdated
      ? response.send(`You are now enrolled`)
      : response.send(`Enrollment unsuccessful. Please try again.`);
  } else {
    return response.send(`You are admin, you can't enroll to a course`);
  }
};
